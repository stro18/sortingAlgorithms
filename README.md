This repository contains implementations for the sorting algorithms  
* HeapSort  
* MergeSort
* QuickSort  

You can call the algorithms like this:  
```java
int[] inputQuicksort = {1, 5, 2, 6, 3, 13, 10};

Sorter quickSorter = new QuickSorter();
quickSorter.sort(inputQuicksort);
```

The class 'Main' contains some basic test cases.