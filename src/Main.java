public class Main {

    public static void main(String[] args)
    {
        // test for quickSort
        System.out.println("QuickSort: ");

        int[] inputQuicksort = {1, 5, 2, 6, 3, 13, 10};

        Sorter quickSorter = new QuickSorter();
        quickSorter.sort(inputQuicksort);

        for (int value: inputQuicksort) {
            System.out.println(value);
        }

        // test for heapSort
        System.out.println("HeapSort: ");

        int[] inputHeapSort = {1, 5, 2, 6, 3, 10, 13, 10};

        Sorter heapSorter = new HeapSorter();
        heapSorter.sort(inputHeapSort);

        for (int value: inputHeapSort) {
            System.out.println(value);
        }

        // test for mergeSort
        System.out.println("MergeSort: ");
        int[] inputMergeSort = {1, 5, 2, 6, 3, 10, 13, 10};

        Sorter mergeSorter = new MergeSorter();
        mergeSorter.sort(inputMergeSort);

        for (int value: inputMergeSort) {
            System.out.println(value);
        }
    }
}
