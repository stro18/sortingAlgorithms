public class HeapSorter implements Sorter {

    @Override
    public void sort(int[] unsorted)
    {
        buildMaxHeap(unsorted);

        for (int i = unsorted.length - 1; i > 0; i--) {
            unsorted[i] = extractMax(unsorted, i);
        }

    }

    private void buildMaxHeap(int[] unsorted)
    {
        for (int i = (int) Math.floor(unsorted.length/2.0); i > -1; i--) {
            maxHeapify(unsorted, i, unsorted.length - 1);
        }
    }

    private int extractMax(int[] unsorted, int lastIndex)
    {
        int max = unsorted[0];

        unsorted[0] = unsorted[lastIndex];
        maxHeapify(unsorted, 0, lastIndex - 1);

        return max;
    }

    private void maxHeapify(int[] unsorted, int startIndex, int lastIndex)
    {
        int maxIndex = startIndex;

        if (startIndex * 2 <= lastIndex && unsorted[startIndex * 2] > unsorted[maxIndex]) {
            maxIndex = startIndex * 2;
        }
        if (startIndex * 2 + 1 <= lastIndex && unsorted[startIndex * 2 + 1] > unsorted[maxIndex]) {
            maxIndex = startIndex * 2 + 1;
        }

        if (maxIndex != startIndex) {
            int tmp = unsorted[startIndex];
            unsorted[startIndex] = unsorted[maxIndex];
            unsorted[maxIndex] = tmp;

            maxHeapify(unsorted, maxIndex, lastIndex);
        }
    }
}
