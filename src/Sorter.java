public interface Sorter {

    void sort(int[] unsorted);
}
