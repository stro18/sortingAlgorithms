public class MergeSorter implements Sorter {

    @Override
    public void sort(int[] unsorted) {
        mergeSort(unsorted, 0, unsorted.length - 1);
    }

    private void mergeSort(int[] unsorted, int first, int last)
    {
        if (first < last) {
            int middle = (first + last) / 2;
            mergeSort(unsorted, first, middle);
            mergeSort(unsorted, middle + 1, last);
            merge(unsorted, first, middle, last);
        }
    }

   private void merge(int[] unsorted, int start, int middle, int end)
    {
        int[] firstHalf = new int[middle - start + 1];
        int[] secondHalf = new int[end - middle];

        for (int i = 0; i < firstHalf.length; i++) {
            firstHalf[i] = unsorted[start + i];
        }
        for (int i = 0; i < secondHalf.length; i++) {
            secondHalf[i] = unsorted[middle + 1 + i];
        }

        int pFirst = 0;
        int pSecond = 0;

        for (int i = start; i <= end; i++) {
            if (pSecond >= secondHalf.length || (pFirst < firstHalf.length && firstHalf[pFirst] <= secondHalf[pSecond])) {
                unsorted[i] = firstHalf[pFirst];
                pFirst++;
            } else {
                unsorted[i] = secondHalf[pSecond];
                pSecond++;
            }
        }
    }
}
