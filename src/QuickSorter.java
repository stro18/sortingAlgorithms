public class QuickSorter implements Sorter {

    @Override
    public void sort(int[] unsorted) {
        quickSort(unsorted, 0, unsorted.length - 1);

    }
    private void quickSort(int[] unsorted, int first, int last)
    {
        if (first < last) {
            int pivotIndex = partition(unsorted, first, last);
            quickSort(unsorted, first, pivotIndex - 1);
            quickSort(unsorted, pivotIndex + 1, last);
        }
    }

    private int partition(int[] unsorted, int first, int last)
    {
        int pivot = unsorted[last];
        int lastSmall = first - 1;

        for(int i = first; i < last; i++){
            if (unsorted[i] < pivot) {
                int temp = unsorted[i];
                unsorted[i] = unsorted[lastSmall + 1];
                unsorted[lastSmall + 1] = temp;
                lastSmall++;
            }
        }

        unsorted[last] = unsorted[lastSmall + 1];
        unsorted[lastSmall + 1] = pivot;

        return lastSmall + 1;
    }
}
